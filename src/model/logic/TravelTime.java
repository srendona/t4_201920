package model.logic;

public class TravelTime implements Comparable<TravelTime> {

	 public int trimestre;
	 
	 public int sourceid;
	 
	 public int distid;
	 
	 public int hod; 
	 
	 public double mean_travel_time;
	 
	 public double standard_deviation_travel_time;
	 
	
	 
	 public TravelTime(int trimestre, int sourceid, int distid, int hod, double mean_travel_time, double standard_deviation_travel_time) {
		 
		this.trimestre = trimestre;
		this.sourceid = sourceid;
		this.distid = distid;
		this.hod = hod;
		this.mean_travel_time = mean_travel_time;
		this.standard_deviation_travel_time = standard_deviation_travel_time;
		
	}

	public int compareTo(TravelTime dato) {
		 int r = 0;
		 
		 if(mean_travel_time > dato.mean_travel_time)
			 r = 1;
		 else if(mean_travel_time < dato.mean_travel_time)
			 r = -1;
		 return r;
	 }

	public int getTrimestre() {
		return trimestre;
	}

	public int getSourceid() {
		return sourceid;
	}

	public int getDistid() {
		return distid;
	}

	public int getHod() {
		return hod;
	}

	public double getMean_travel_time() {
		return mean_travel_time;
	}

	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}

	
}
