package model.logic;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import com.opencsv.CSVReader;

import model.data_structures.MaxColaCP;
import model.data_structures.MaxHeapCP;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo <T extends Comparable<T>> {
	/**
	 * Atributos del modelo del mundo
	 */
	private MaxHeapCP<T > datosHeap;
	private MaxColaCP<T> datosCola;
	
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datosHeap=new MaxHeapCP<>();
		datosCola=new MaxColaCP<>();
	}
	
	
	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */

	
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 * @throws IOException 
	 */
	
	public void loadTravelTimes() throws IOException {
		String[] line=null;
		CSVReader reader= new CSVReader(new FileReader(new File("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv")));
		reader.readNext();
		int cont=0;
		while((line = reader.readNext()) != null )
		{
			TravelTime dato= new TravelTime(1, Integer.parseInt(line[0]), Integer.parseInt(line[1]), Integer.parseInt(line[2]), Double.parseDouble(line[3]), Double.parseDouble(line[4]));
			datosCola.agregar((T) dato);
			datosHeap.agregar((T) dato);
			cont++;
		}
		line=null;
		reader= new CSVReader(new FileReader(new File("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv")));
		reader.readNext();
		while((line = reader.readNext()) != null )
		{
			TravelTime dato= new TravelTime(2, Integer.parseInt(line[0]), Integer.parseInt(line[1]), Integer.parseInt(line[2]), Double.parseDouble(line[3]), Double.parseDouble(line[4]));
			datosCola.agregar((T) dato);
			datosHeap.agregar((T) dato);
			cont++;
		}
		System.out.println("Se cargaron exitosamente "+cont+" viajes.");
		
		
		
		
		
		
	}
	public int darTamano()
	{
		return datosHeap.darNumElementos();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregarHeap(T dato)
	{	
		datosHeap.agregar(dato);
	}
	
	public void agregarCola(T dato)
	{	
		datosCola.agregar(dato);
	}
	
	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public T darMax()
	{
		return (T) datosHeap.darMax();
	}
	
	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public T sacarMaxHeap()
	{
		return (T) datosHeap.sacarMax();
	}
	public T sacarMaxCola()
	{
		return (T) datosCola.sacarMax();
	}
	public void generarMuestra(int N)
	{
		long t=System.currentTimeMillis();
		MaxHeapCP<T> heapMuestra=new MaxHeapCP<>();
		Random random = new Random();
		int randomInt=0;
		for(int i=0; i<N;i++)
		{
			randomInt = Math.abs(random.nextInt());
			if(randomInt>N) i--;
			else 
			{
				heapMuestra.agregar(datosHeap.buscar(randomInt));
			}
		}
		t=System.currentTimeMillis()-t;
		//System.out.println("Muestra generada en heap. Tiempo: "+t+"ms");
		t=System.currentTimeMillis();
		MaxColaCP colaMuestra=new MaxColaCP();
		for(int i=0; i<N;i++)
		{
			randomInt = Math.abs(random.nextInt());
			if(randomInt>N) i--;
			else 
			{
				colaMuestra.agregar(datosCola.buscar(randomInt));
			}
		}
		t=System.currentTimeMillis()-t;
		System.out.println("Muestra generada en cola. Tiempo: "+t+ "ms");
	}

	public T buscar(int i) {
		return datosCola.buscar(i);
	}
}

