package model.data_structures;

import model.logic.TravelTime;

public class MaxHeapCP <T extends Comparable<T>>{
	private T[] keys;
	private int N;
	private int tamanoMax;
	
	
	
	public MaxHeapCP() {
		keys= (T[]) new TravelTime[200001];
		N=0;
		tamanoMax=200001;
	}
	
	public int darNumElementos() {
		return N;
	}
	
	public void agregar(T elemento) {
		if(tamanoMax==N+1)
		{
			tamanoMax = 2 * tamanoMax;
            T [ ] copia = keys;
            keys = (T[]) new TravelTime[tamanoMax];
            for ( int i = 1; i <= N; i++)
            {
             	 keys[i] = copia[i];
             	 
            } 
    	    System.out.println("Arreglo lleno: " + N + " - Arreglo duplicado: " + tamanoMax);
    	    copia=null;
		}
       keys[++N]=elemento;
       swim(N);
	}
	private void swim(int k)
	{
		while(k>1 && less(k/2,k))
		{
			exch(k,k/2);
			k=k/2;
		}
	}
	private boolean less(int a, int b)
	{
		if(keys[a].compareTo(keys[b])<0)
		{
			return true;
		}
		else return false;
	}
	private void exch(int a, int b)
	{
		T aux=keys[b];
		keys[b]=keys[a];
		keys[a]=aux;
	}
	
	public T darMax () {
		T r = keys[1];
		return r;
	}
	
	public T sacarMax() {
		T r = keys[1];
		exch(1,N--);
		sink(1);
		keys[N+1]=null;
		return r;
	}
	private void sink(int a)
	{
		while(2*a<=N)
		{
			int j=2*a;
			if(j<N && less(j, j+1))
				j++;
			if(!less(a,j))
				break;
			exch(a,j);
			a=j;
		}
	}
	
	public boolean esVacia () {
		if(darMax()!=null) return false;
		return true;
	}
	public T buscar(int k)
	{
		return keys[k];
	}
}
