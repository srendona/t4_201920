package model.data_structures;

import model.logic.TravelTime;

@SuppressWarnings("unchecked")
public class MaxColaCP <T extends Comparable<T>>{
	/**
	 * Capacidad maxima del arreglo
	 */
    private int tamanoMax;
    
	private T[] pq;
	
	private int n;

	public MaxColaCP() {
		pq = (T[]) new TravelTime[200001];
		n = 0;
		tamanoMax = 200001;
	}
	
	public int darNumElementos() {
		return n;
	}
	
	public void agregar(T elemento) {

		int m = 0;
		 if ( n == tamanoMax )
         {  // caso de arreglo lleno (aumentar tamaNo)
              tamanoMax = 2 * tamanoMax;
              T [ ] copia = pq;
              pq = (T[]) new TravelTime[tamanoMax];
              for ( int i = 0; i < n; i++)
              {
            	  pq[i] = copia[i];
              } 
      	  //  System.out.println("Arreglo lleno: " + pq + " - Arreglo duplicado: " + tamanoMax);
         }	

         /*for ( int i = 0; i < n; i++)
         {
        	 if(!less(m, i)) {
        		 m = i;
        	 }
         }
         if(n>0) {
             for(int i = n-1; i < tamanoMax; i++) {
            	 pq[n] = pq[n-1];
             }
         }*/

		 pq[n] = elemento;
         n++;
	}
	
	public T sacarMax () {
		int max = 0;
		if(n == 0)
			return null;
		
		for(int i = 0; i < n; i++) {
			if(((TravelTime) pq[max]).compareTo(((TravelTime) pq[i]))<0)
				 max = i; exch(max, n-1);
		}
		return pq[--n];
	}
	
	public T darMax() {
		int max = 0;
		if(n == 0)
			return null;
		
		for(int i = 0; i < n; i++) {
			if(((TravelTime) pq[max]).compareTo(((TravelTime) pq[i]))<0)
				 max = i; 
		}
		return pq[max];
	}
	
	public boolean esVacia () {
		return n == 0;
	}
	
	public void exch(int pos1, int pos2)
	{
		T aux =pq[pos1];
		pq[pos1]=pq[pos2];
		pq[pos2]=aux;
	}

	private boolean less(int a, int b)
	{
		if(pq[a].compareTo(pq[b])<0)
		{
			return true;
		}
		else return false;
	}
	public T buscar(int k) {
		// TODO Auto-generated method stub
		return pq[k];
	}
}
