package test.data_structures;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.MaxColaCP;
import model.data_structures.MaxHeapCP;
import model.logic.MVCModelo;

public class PruebaColaPrioridad<T> {
	
	private MaxHeapCP datosHeap;
	private MaxColaCP datosCola;
	
	@Before
	public void setUp() throws Exception {
		
	}
	

	@Test
	public void testAgregarCola() throws Exception {
		setUp();
		MVCModelo m = new MVCModelo();
		m.loadTravelTimes();
		m.generarMuestra(200000);
		MVCModelo x = new MVCModelo();
		long t = System.currentTimeMillis();
		for(int i = 0; i < m.darTamano(); i++)
			x.agregarCola(m.buscar(i));
		t= System.currentTimeMillis()-t;
		System.out.println("AgregarCola: "+t);
		m = new MVCModelo();
	}
	@Test
	public void testSacarMaxCola() throws Exception {
		setUp();
		MVCModelo m = new MVCModelo();
		m.loadTravelTimes();
		m.generarMuestra(200000);
		long t = System.currentTimeMillis();
		for(int i = 0; i < m.darTamano(); i++) {
			m.sacarMaxCola();
		}
		
		t= System.currentTimeMillis()-t;
		System.out.println("SacarMaxCola: "+t);
		m = new MVCModelo();
	}
	
	@Test
	public void testAgregarHeap() throws Exception {
		setUp();
		MVCModelo m = new MVCModelo();
		m.loadTravelTimes();
		m.generarMuestra(200000);
		MVCModelo x = new MVCModelo();
		long t = System.currentTimeMillis();
		for(int i = 0; i < m.darTamano(); i++)
			x.agregarHeap(m.buscar(i));
		t= System.currentTimeMillis()-t;
		System.out.println("AgregarHeap: "+t);
		m = new MVCModelo();
	}
	@Test
	public void testSacarMaxHeap() throws Exception {
		setUp();
		MVCModelo m = new MVCModelo();
		m.loadTravelTimes();
		m.generarMuestra(200000);
		long t = System.currentTimeMillis();
		for(int i = 0; i < m.darTamano(); i++) {
			m.sacarMaxHeap();
		}
		t= System.currentTimeMillis()-t;
		System.out.println("SacarMaxHeap: "+t);
		m = new MVCModelo();
	}
}
